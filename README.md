<h1 align="center"> Instrumentação SpringBoot.</h1>

Exemplo de instrumentação para inclusão de TraceID nos logs e exportação de métricas para o Jaeger e Prometheus

## A seguinte documentação se refere à uma aplicação utilizando Springboot e um banco de dados MySQL.

Ferramentas utilizadas:

- Maven
- OpenTelemetry v1.11.1
- Prometheus
- Jaeger

</br>

## 1. Alterações na aplicação.
</br>

Adicionar as dependencias do OpenTelemetry ao pom.xml (caso compilada com o Maven):


    <dependencies>
      <dependency>
        <groupId>io.opentelemetry.instrumentation</groupId>
        <artifactId>opentelemetry-logback-1.0</artifactId>
        <version>1.9.2-alpha</version>
        <scope>runtime</scope>
      </dependency>
      <dependency>
            <groupId>io.opentelemetry</groupId>
            <artifactId>opentelemetry-sdk</artifactId>
            <version>1.10.1</version>
      </dependency>
    </dependencies>


Criar/atualizar o arquivo de configuração logback.xml no diretório _resources_:

    <?xml version="1.0" encoding="UTF-8" ?>
    <configuration>
        <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
            <encoder>
                   <pattern>
                        %date{HH:mm:ss.SSS} [%thread] traceID: %X{trace_id} %-5level %logger{36} - %msg\n
                   </pattern>
            </encoder>
        </appender>
        <appender name="OTEL" class="io.opentelemetry.instrumentation.logback.v1_0.OpenTelemetryAppender">
            <appender-ref ref="STDOUT" />
        </appender>
        <root>
            <level value="DEBUG" />
            <appender-ref ref="STDOUT" />
        </root>
    </configuration>
</br>

## 2. Executando a aplicação junto ao opentelemetry:

Para executar a aplicação, utilize o Dockerfile:

```
FROM openjdk:11-buster
WORKDIR /app
RUN apt update -y && apt install curl wget -y
RUN wget https://repo1.maven.org/maven2/io/opentelemetry/javaagent/opentelemetry-javaagent/1.11.1/opentelemetry-javaagent-1.11.1.jar -O opentelemetry-javaagent.jar

COPY target/*.jar .
COPY entrypoint.sh .
ENTRYPOINT ["./entrypoint.sh"]
```
_Obs: A aplicação deverá se encontrar no diretório /target, que fica dentro do diretório onde o Dockerfile está._

E o script _entrypoint.sh_ deve estar no mesmo diretório do Dockerfile e definido da seguinte maneira:
```
#!/bin/sh 
set -e

java ${JAVA_ARGS} -jar SpringBootRegistrationLogin-1.0.jar
```


A variável **JAVA_ARGS** deve ser definida com os seguintes argumentos:

```
-javaagent:PATH/TO/opentelemetry-javaagent.jar -Dotel.traces.exporter=jaeger -Dotel.exporter.jaeger.endpoint=JAEGER_URL -Dotel.exporter.jaeger.service.name=SERVICE_NAME -Dotel.metrics.exporter=prometheus -Dotel.exporter.prometheus.port=METRICS_PORT -Dotel.exporter.prometheus.host=0.0.0.0

```

Defina-os da seguinte forma:

```
-javaagent:PATH/TO/opentelemetry-javaagent.jar <-- Em PATH/TO, defina o local onde se encontra o agente do opentelemetry. Ex: /home/user/opentelemetry-javaagent.jar
-Dotel.traces.exporter=jaeger  <-- Defina para qual exporter será enviado as métricas (neste caso será o Jaeger).
-Dotel.exporter.jaeger.endpoint=JAEGER_IP <-- IP onde está o Jaeger. Ex: 192.168.0.1:8000
-Dotel.exporter.jaeger.service.name=SERVICE_NAME <-- Nome do serviço que irá aparecer no Jaeger. Ex: SpringBoot-APP
-Dotel.metrics.exporter=prometheus <-- Defina para qual exporter será enviado as métricas. Ex: Prometheus
-Dotel.exporter.prometheus.port=METRICS_PORT <-- Qual porta será utilizada na aplicação para a saída das métricas. Ex: 9464
-Dotel.exporter.prometheus.host=METRICS_IP <-- Qual IP do host será utlizado para expor as métricas. Neste caso defina-o como 0.0.0.0 para que seja qualquer IP disponível no host. 

```



## 3. Inclusão do job da aplicação no Prometheus:
</br>


Incluir no arquivo prometheus.yml:

Obs: Em METRICS_IP:METRICS_PORT, utilize o parâmetros prometheus.port e .host definidos no passo anterior.

```
- job_name: JOB_NAME
  scrape_interval: 5s
  metrics_path: /metrics

  static_configs:
    - targets: ['METRICS_IP:METRICS_PORT']
      labels:
        env: 'ENVIRONMENT'
```


## Referências:

Traceid: https://help.sumologic.com/Traces/Getting_Started_with_Transaction_Tracing/Instrument_your_application_with_OpenTelemetry/Java_OpenTelemetry_auto-instrumentation/TraceId_and_SpanId_injection_into_logs_configuration

OpenTelemetry: https://opentelemetry.io/docs/instrumentation/java/automatic/

Jaeger: https://www.jaegertracing.io/docs/1.31/getting-started/

Prometheus: https://prometheus.io/docs/prometheus/latest/getting_started/

https://github.com/open-telemetry/opentelemetry-java/blob/main/sdk-extensions/autoconfigure/README.md#prometheus-exporter

